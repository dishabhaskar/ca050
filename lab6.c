#include <stdio.h>

void avg(int a[10],int n)
{
    int sum=0;
    float average=0.0;
    for(int i=0;i<n;i++)
    {
        sum=sum+a[i];
    }
    average=(float)sum/n;
    printf("the average of the array elements are=%f",average);
}

int main()
{
    int n,a[10];
    printf("enter the number of elements \n");
    scanf("%d",&n);
    printf("enter the elements of the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    avg(a,n);
    return 0;
}